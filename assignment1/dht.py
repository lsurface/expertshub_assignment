import sys
import Adafruit_DHT
import RPi.GPIO as GPIO
from time import sleep
import mail
sensor = Adafruit_DHT.DHT11
pin = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin,GPIO.OUT)

try:
	while True:
		hum, temp = Adafruit_DHT.read_retry(sensor,pin)
		if temp is not None and hum is not None:
			print(str(hum)+" %   "+str(temp)+" Celsius")
			message = """
				Humidity: """ +str(hum)+"""% and Temperature
				"""+str(temp)+""" Celsius"""
			mail.mail(message)
		else:
			print('Cannot read value from sensor');
		
		sleep(2) #Sleeps for 2 seconds

except KeyboardInterrupt:
	GPIO.cleanup()
	print('GPIO cleanup')
