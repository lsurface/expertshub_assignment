import RPi.GPIO as GPIO
import things_speak as server
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()
choice_1 = 2
choice_2 = 3

GPIO.setup(choice_1,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(choice_2,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)


def button_callback(channel):
	
	if channel == 2:
		field = 'field1'
	elif channel == 3:
		field = 'field2'
	
	server.write_server(field)	
	print('Pushed')


GPIO.add_event_detect(choice_1,GPIO.RISING,callback=button_callback)
GPIO.add_event_detect(choice_2,GPIO.RISING,callback=button_callback)

msg = raw_input('Press Enter to exit\n')
GPIO.cleanup()
print('GPIO cleanup\n')


