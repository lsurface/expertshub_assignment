import RPi.GPIO as GPIO
import things_speak as server
import ultrasonic as dist
from time import sleep

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

led1 = 3
led2 = 5
led3 = 7

GPIO.setup(led1,GPIO.OUT)
GPIO.setup(led2,GPIO.OUT)
GPIO.setup(led3,GPIO.OUT)

marker = 0

#length of container
length = 100
try:
	while True:
		distance = dist.distance()

		#Since sensor is on top, subtraction is required to get water level from bottom
		level = length - distance

		#Updating online only if there is a change in water level
		if marker != level:
			server.write_server(level)
		marker = level
		if level >=  80:
			GPIO.output(led1,1)
			GPIO.output(led2,1)
			GPIO.output(led3,1)
		elif level  >= 50:
			GPIO.output(led1,0)
			GPIO.output(led2,1)
			GPIO.output(led3,1)
		elif level >= 10:
			GPIO.output(led1,0)
			GPIO.output(led2,0)
			GPIO.output(led3,1)
		else:
			GPIO.output(led1,0)
			GPIO.output(led2,0)
			GPIO.output(led3,0)

		sleep(1)

except KeyboardInterrupt:
	GPIO.cleanup()
	print('GPIO Cleanup')
