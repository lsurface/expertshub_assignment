import RPi.GPIO as GPIO
import time
 
GPIO.setmode(GPIO.BCM)
 
trigger = 18
echo = 24
 
GPIO.setup(trigger, GPIO.OUT)
GPIO.setup(echo, GPIO.IN)
 
def distance():
    
    GPIO.output(trigger, True)
 
    time.sleep(0.00001)
    GPIO.output(echo, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
   
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()
 
   
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()
 
   
    TimeElapsed = StopTime - StartTime
   
   
    distance = (TimeElapsed * 34300) / 2
 
    return distance
